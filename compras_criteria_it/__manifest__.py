# -*- encoding: utf-8 -*-
{
    'name': 'compras criteria',
    'category': 'purchase',
    'author': 'ITGRUPO',
    'depends': ['purchase'],
    'version': '1.0',
    'description':"""
     compras criteria
    """,
    'auto_install': False,
    'demo': [],
    'data': [
        'security/ir.model.access.csv',
        'views/purchase_order.xml',
        'data/secuence.xml'
        ],
    'installable': True
}