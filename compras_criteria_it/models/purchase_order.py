from odoo import models, fields, api
class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'
    transportista = fields.Many2one('res.partner')
    guia_prov     = fields.Char(string="Guia del Proveedor")
    guia_tran     = fields.Char(string="Guia del Transportsta")
    name          = fields.Char(readonly="0")

class PurchaseOrderLine(models.Model):
    _inherit = 'purchase.order.line'
    tipo_envase = fields.Many2one('tipo.envase.purchase')
    num_envases = fields.Integer(string="# Envase")
    lote_prov   = fields.Char()
    lote_auto   = fields.Char(string='# Lote autogenerado')
    num_recep   = fields.Char(string='# Recepciòn')
    date_ingreso = fields.Date(string="Fecha de Ingreso")
    estado       = fields.Selection([('quarantine','cuarentena'),('cancel','rechazazado'),
                                     ('approved1','aprobado 1'),('approved2','aprobado 2'),
                                     ('posted','contabilizado')],'quarantine')
    obs         =  fields.Char(string="Observaciòn")
    #@api.onchange('product_id')
    '''
    def change_lote_auto(self):
        secuencia = self.env['ir.sequence'].search([('code','=','criteria')])
        for s in self:
            s.lote_auto = 'hola'

            sc = 'hhh'
            if secuencia:
                #secuencia = secuencia.id
                sc = str(s.id)
                sc = str(secuencia[0].prefix)+str(s.id)
            else:
                sc = str(s.id)
            s.lote_auto = sc  
    '''

class TipoEnvase(models.Model):
    _name  = 'tipo.envase.purchase'


